import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { DivisasInterface } from '../interfaces/ultimosValores.interface';
import { Observable } from "rxjs";


@Injectable({
    providedIn: 'root'
})

export class BuscadoresService {

    constructor( private readonly http: HttpClient){}

    ultimosValores(): Observable<DivisasInterface[]>{
        const url = environment.ultimosValores;

        return this.http.get<DivisasInterface[]>( url );
    }

    porMeracdo(divisa: string): Observable<DivisasInterface>{
        const url = `${environment.porMercado}/${divisa}`;

        return this.http.get<DivisasInterface>(url);
    }

    porFecha(divisa: string, fecha:string){
        const url = `${environment.porFecha}/${divisa}/${fecha}`;

        return this.http.get<DivisasInterface>(url);

    }
}