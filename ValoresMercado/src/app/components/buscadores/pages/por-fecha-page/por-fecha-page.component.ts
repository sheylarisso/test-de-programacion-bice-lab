import { Component } from '@angular/core';
import { BuscadoresService } from '../../services/buscadores.service';
import { DivisasInterface } from '../../interfaces/ultimosValores.interface';

@Component({
  selector: 'app-por-fecha-page',
  templateUrl: './por-fecha-page.component.html',
  styleUrls: ['./por-fecha-page.component.scss']
})
export class PorFechaPageComponent{
  
  divisaSeleccionada: string = null;
  fechaSeleccionada:string = null;
  verInformacion = false;
  dataDivisa: DivisasInterface[]= [];
  
  constructor(private readonly buscadoresService: BuscadoresService){}


  buscar(){
    if(this.divisaSeleccionada != null && this.fechaSeleccionada != null){
      console.log(this.divisaSeleccionada);
      console.log(this.fechaSeleccionada);
      
      
      this.buscadoresService.porFecha(this.divisaSeleccionada, this.fechaSeleccionada)
        .subscribe( valor => {
          this.dataDivisa = [valor];
          this.verInformacion = true;
        })
    }
  }
}
