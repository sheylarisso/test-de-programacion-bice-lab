import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PorFechaPageComponent } from './por-fecha-page.component';

describe('PorFechaPageComponent', () => {
  let component: PorFechaPageComponent;
  let fixture: ComponentFixture<PorFechaPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PorFechaPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PorFechaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
