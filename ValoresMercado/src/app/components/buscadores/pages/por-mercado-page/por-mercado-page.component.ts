import { Component } from '@angular/core';
import { BuscadoresService } from '../../services/buscadores.service';
import { DivisasInterface } from '../../interfaces/ultimosValores.interface';

@Component({
  selector: 'app-por-mercado-page',
  templateUrl: './por-mercado-page.component.html',
  styleUrls: ['./por-mercado-page.component.scss']
})
export class PorMercadoPageComponent{

  constructor( private readonly buscadoresService: BuscadoresService) { }

  dataDivisa: DivisasInterface[]= [];
  verInformacion : boolean = false;


  valorPorMercado(divisa: string){
    this.buscadoresService.porMeracdo(divisa)
      .subscribe( valor => {
        if(valor){
          const value =Object.values(valor.values)[0];
          valor.value = value;
          this.dataDivisa = [valor];
          this.verInformacion = true;
        }
      })
  }

}
