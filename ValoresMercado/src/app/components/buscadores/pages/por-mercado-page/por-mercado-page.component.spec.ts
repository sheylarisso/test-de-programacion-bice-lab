import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PorMercadoPageComponent } from './por-mercado-page.component';

describe('PorMercadoPageComponent', () => {
  let component: PorMercadoPageComponent;
  let fixture: ComponentFixture<PorMercadoPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PorMercadoPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PorMercadoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
