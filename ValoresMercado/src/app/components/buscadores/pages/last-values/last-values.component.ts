import { Component, OnInit } from '@angular/core';
import { BuscadoresService } from '../../services/buscadores.service';
import { DivisasInterface } from '../../interfaces/ultimosValores.interface';

@Component({
  selector: 'app-last-values',
  templateUrl: './last-values.component.html',
  styleUrls: ['./last-values.component.scss']
})
export class LastValuesComponent implements OnInit {

  constructor( private readonly buscadoresService: BuscadoresService) { }

  ultimosValores: DivisasInterface[] = [];
  verTabla: boolean = false;

  ngOnInit(): void {
    this.lastValues();
  }

  lastValues(){
    this.buscadoresService.ultimosValores()
      .subscribe( valores => {
        if(valores){
          this.ultimosValores = valores;
          this.verTabla = true;
        }
      })
  }

}
