export interface DivisasInterface {
    key:   string;
    name:  string;
    unit:  UnidadInterface
    date:  number;
    value?: number;
    values? :  |  { [key: string]: number }
}

export enum UnidadInterface {
    Dolar = "dolar",
    Pesos = "pesos",
    Porcentual = "porcentual",
}
