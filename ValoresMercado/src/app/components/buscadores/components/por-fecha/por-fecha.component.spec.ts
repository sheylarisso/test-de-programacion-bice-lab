import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PorFechaComponent } from './por-fecha.component';

describe('PorFechaComponent', () => {
  let component: PorFechaComponent;
  let fixture: ComponentFixture<PorFechaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PorFechaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PorFechaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
