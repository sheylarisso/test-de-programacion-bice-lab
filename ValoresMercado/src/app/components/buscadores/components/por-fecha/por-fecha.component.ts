import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-por-fecha',
  templateUrl: './por-fecha.component.html',
  styleUrls: ['./por-fecha.component.scss']
})
export class PorFechaComponent implements OnInit {

  @Output() onFecha : EventEmitter<string> = new EventEmitter();
  calendarioModel: FormGroup;

  calendarioControl:FormControl;

  input;

  constructor( ) {
    this.calendarioControl = new FormControl('');
   }
  
  ngOnInit(): void {
    this.calendarioControl.valueChanges.subscribe( fecha => {
      if(fecha != null){
        const date = moment(fecha).format('DD-MM-YYYY');
        this.onFecha.emit(date)
      }
    })
  }
}
