import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UltimosValoresComponent } from './ultimos-valores.component';

describe('UltimosValoresComponent', () => {
  let component: UltimosValoresComponent;
  let fixture: ComponentFixture<UltimosValoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UltimosValoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UltimosValoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
