import { Component, Input, OnChanges } from '@angular/core';
import { DivisasInterface } from '../../interfaces/ultimosValores.interface';

@Component({
  selector: 'app-ultimos-valores',
  templateUrl: './ultimos-valores.component.html',
  styleUrls: ['./ultimos-valores.component.scss']
})
export class UltimosValoresComponent implements OnChanges{
  
  @Input() lastValues: DivisasInterface[];

  data: Object;
  
  ngOnChanges(): void {
    this.data = Object.values(this.lastValues)
  }


}
