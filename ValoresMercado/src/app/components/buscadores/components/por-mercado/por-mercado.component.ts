import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-por-mercado',
  templateUrl: './por-mercado.component.html',
  styleUrls: ['./por-mercado.component.scss']
})
export class PorMercadoComponent {

  @Output() onBuscarDivisa: EventEmitter<string> = new EventEmitter();
  divisas: string[] = ['cobre','dolar','euro','ipc','ivp','oro','plata','uf','utm','yen'];
  divisaActiva: string = '';

  getClaseCSS( divisa: string): string{
    return ( divisa === this.divisaActiva) ? 'btn btn-primary': 'btn btn-outline-primary';
  }

  buscarDivisa( divisa: string){
    if( divisa === this.divisaActiva) { return;}
    this.divisaActiva = divisa;
    this.onBuscarDivisa.emit(divisa)
  }
}
