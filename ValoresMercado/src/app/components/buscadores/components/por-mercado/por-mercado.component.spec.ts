import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PorMercadoComponent } from './por-mercado.component';

describe('PorMercadoComponent', () => {
  let component: PorMercadoComponent;
  let fixture: ComponentFixture<PorMercadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PorMercadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PorMercadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
