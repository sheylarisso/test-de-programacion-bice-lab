import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UltimosValoresComponent } from './components/tabla-ultimos-valores/ultimos-valores.component';
import { PorMercadoComponent } from './components/por-mercado/por-mercado.component';
import { PorFechaComponent } from './components/por-fecha/por-fecha.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LastValuesComponent } from './pages/last-values/last-values.component';
import { PorMercadoPageComponent } from './pages/por-mercado-page/por-mercado-page.component';
import { PorFechaPageComponent } from './pages/por-fecha-page/por-fecha-page.component';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';


@NgModule({
  declarations: [
    UltimosValoresComponent,
    PorMercadoComponent,
    PorFechaComponent,
    LastValuesComponent,
    PorMercadoPageComponent,
    PorFechaPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    ReactiveFormsModule
  ],
  exports: [
    PorMercadoPageComponent,
    LastValuesComponent,
    PorFechaPageComponent
  ]
})
export class BuscadoresModule { }
