import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LastValuesComponent } from './components/buscadores/pages/last-values/last-values.component';
import { PorMercadoPageComponent } from './components/buscadores/pages/por-mercado-page/por-mercado-page.component';
import { PorFechaPageComponent } from './components/buscadores/pages/por-fecha-page/por-fecha-page.component';

const routes: Routes = [
  {
    path: '',
    component: LastValuesComponent,
    pathMatch: 'full'
},
{
    path: 'last',
    component: LastValuesComponent
},
{
    path: 'date',
    component: PorFechaPageComponent
},
{
    path: 'search/:id',
    component: PorMercadoPageComponent
},
{
    path: '**',
    redirectTo: ''
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
