const url = 'https://indecon.site';

export const environment = {
  production: true,
  ultimosValores : `${url}/last`,
  porMercado: `${url}/values`,
  porFecha: `${url}/date`,
};
