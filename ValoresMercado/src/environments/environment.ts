const url = 'https://indecon.site';

export const environment = {
  production: false,
  ultimosValores : `${url}/last`,
  porMercado: `${url}/values`,
  porFecha: `${url}/date`,
};


